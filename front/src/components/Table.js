import React, {Component} from "react";
import MaterialTable from 'material-table';
import Form from "./Form";

export default class Table extends Component {
   constructor(props) {
    super(props);
    this.state = {
      showCreate:false,
      showEdit:false,
      id:{},
    }
    this.tableRef = React.createRef();

  }


  _handleCloseCreate = () => {
    this.setState({showCreate: false});
  }
  
  _handleShowCreate = () => {
    this.setState({showCreate: true});
  }

  _handleCloseEdit = () => {
    this.setState({showEdit: false});
  }
  
  _handleShowEdit = (rowData) => {
    this.setState({id:rowData,showEdit: true});
  }

  render() {
    var principal
        if (this.state.showCreate){
            principal=<Form edit={false} url={this.props.url} cerrar={this._handleCloseCreate}/>
        }else if(this.state.showEdit){
            principal=<Form edit={true} data={this.state.id} url={this.props.url} cerrar={this._handleCloseEdit}/> 
        }else{
          principal=
            <MaterialTable
              title="Empleados"
              tableRef={this.tableRef}
              columns={[
                { title: 'Nombre', field: 'nombre'},
                { title: 'Apellido', field: 'apellido'},
                { title: 'Email', field: 'email'},
                { title: 'Teléfono', field: 'telefono'},
              ]}
              data={
                query =>
                new Promise((resolve, reject) => {
                  let url = this.props.url
                  url += '?page=' + (query.page + 1)
                  url += '&per_page=' + query.pageSize
                  url += '&search='+query.search
                  url += '&filters='+JSON.stringify(query.filters)
                  //url += '&dates='+JSON.stringify(dates)
                  console.log(url)
                  fetch(url)
                    .then(response => response.json())
                    .then(result => {
                      console.log(result)
                       if(result.meta.to == null){
                        resolve({
                          data: result.data,
                          page: result.meta.current_page - 2,
                          totalCount: result.meta.total,
                        })
                        this.tableRef.current && this.tableRef.current.onQueryChange()
                      }else{ 
                        resolve({
                          data: result.data,
                          page: result.meta.current_page - 1,
                          totalCount: result.meta.total,
                        })
                      }
                    })
                })
              }
              actions={[
                {
                  icon: 'add',
                  tooltip: 'Nuevo empleado',
						      iconProps:{color:'primary'},
                  isFreeAction: true,
                  onClick: (event) => {
                    console.log("Onclik");
                    this._handleShowCreate();
                  }
                },
                rowData => ({
                  icon: 'edit',
                  tooltip: 'Editar empleado',
                  onClick: (event, rowData) => {
                    this._handleShowEdit(rowData);
                  },
                  disabled: rowData.editable === 0
                }),
                rowData => ({
                  icon: 'delete',
                  tooltip: 'Eliminar empleado',
                  onClick: (event, rowData) => {
                    if ( window.confirm("¿Seguro que desea eliminar los datos del empleado: " + rowData.apellido+" "+rowData.nombre+"?")){
                        //var data = new FormData();
                        var misCabeceras = new Headers();
                        misCabeceras.append('X-Requested-With', 'XMLHttpRequest');
                        //misCabeceras.append('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));
                        var miInit = { method: 'DELETE',
                                    headers: misCabeceras,
                                    //body: data, 
                        };
                        console.log(miInit,this.props.url+"/"+rowData.id);
                        fetch(this.props.url+"/"+rowData.id,miInit)
                          .then(response =>  {
                            if (response.ok){
                              return response.json() 
                            }else{
                              throw response.json()
                            }
                          }) 
                          .then(result => {
                            console.log("Exito",result);
                            this.tableRef.current && this.tableRef.current.onQueryChange()
                          }).catch((e) =>  {
                            e.then(item=>console.error("Error",item))
                          }) 
                      }
                    }
                })
              ]}      
              options={{
                sorting: false,
                actionsColumnIndex: -1,
                search: true,
                headerStyle: { backgroundColor: '#EDEDED'},
                pageSizeOptions: [5, 10, 20, 50],
                filtering: true,
              }}
              localization={{
                body: {
                  //dateTimePickerLocalization: Locale,
                  emptyDataSourceMessage: 'No hay datos que mostrar',
                  filterRow:{
                    filterTooltip:'Filtro'
                  }
                },
                toolbar: {
                  searchTooltip: 'Buscar',
                  searchPlaceholder: 'Buscar'
                },
                header:{
                actions:'Acciones',
                },
                pagination: {
                  labelRowsSelect: 'filas',
                  labelDisplayedRows: ' {from}-{to} de {count}',
                  firstTooltip: 'Primera Página',
                  previousTooltip: 'Página Previa',
                  nextTooltip: 'Siguiente Página',
                  lastTooltip: 'Ultima Página'
                }
              }}
            />
        }
      return(
        principal
      );
    }
  }