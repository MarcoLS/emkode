import React, {Component} from "react";
// material-ui
import PropTypes from 'prop-types';
// @material-ui/icons

// core components
import Grid from "@material-ui/core/Grid";
import Divider from '@material-ui/core/Divider';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';


class Form extends Component{
  constructor(props) {
    super(props);
    if(this.props.data){
      this.state = {
        nombre: this.props.data.nombre,
        apellido: this.props.data.apellido,
        email: this.props.data.email,
        telefono: this.props.data.telefono,
        id:this.props.data.id ,
        errors:[]
      };
    }else{
      this.state = {
        nombre: "",
        apellido: "",
        email: "",
        telefono:"" ,
        id:"" ,
        errors:[]
      };
    }

    this.__handleSubmitEdit = this.__handleSubmitEdit.bind(this);
    this.__handleSubmitCreate = this.__handleSubmitCreate.bind(this);
    this.__handleChangeNombre = this.__handleChangeNombre.bind(this);
    this.__handleChangeApellido = this.__handleChangeApellido.bind(this);
    this.__handleChangeEmail = this.__handleChangeEmail.bind(this);
    this.__handleChangeTelefono = this.__handleChangeTelefono.bind(this);
  }
  
	
  __handleChangeNombre(event) {
    this.setState({nombre: event.target.value});
  }

  __handleChangeApellido(event) {
    this.setState({apellido: event.target.value});
  }

  __handleChangeEmail(event) {
    this.setState({email: event.target.value});
  }

  __handleChangeTelefono(event) {
    this.setState({telefono: event.target.value});
  }

  __handleSubmitCreate(event){

    event.preventDefault();
    //alert("Se accedio al metodo "+this.state.descripcion+" "+this.state.costo+" "+this.state.fecha);
    var data = new FormData();
    data.append("nombre",this.state.nombre)
    data.append("apellido",this.state.apellido)
    data.append("email",this.state.email)
    data.append("telefono",this.state.telefono)
      
    var misCabeceras = new Headers();
    misCabeceras.append('X-Requested-With', 'XMLHttpRequest');
    //misCabeceras.append('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));
    
    var miInit = { method: 'POST',
                headers: misCabeceras,
                body: data,
                //mode: 'cors',
                //cache: 'default' 
    };
    console.log(miInit,this.props.url);
      fetch(this.props.url,miInit)
        .then(response =>  {
          if (response.ok){
            return response.json() 
          }else{
            throw response.json()
          }
        }) 
        .then(result => {
          console.log("Exito",result);
          this.props.cerrar();
        }).catch((e) =>  {
          e.then(item=>{
            this.setState({errors:item["errors"]})
            console.error("Error",item)}
          )
        })
  }
  
  __handleSubmitEdit(event){

    event.preventDefault();

    var data = new FormData();
    data.append("nombre",this.state.nombre)
    data.append("apellido",this.state.apellido)
    data.append("email",this.state.email)
    data.append("telefono",this.state.telefono)

    var url = this.props.url+"/"+this.state.id
    url += "?nombre="+this.state.nombre
    url += "&apellido="+this.state.apellido
    url += "&email="+this.state.email
    url += "&telefono="+this.state.telefono

    var misCabeceras = new Headers();
    misCabeceras.append('X-Requested-With', 'XMLHttpRequest');
    //misCabeceras.append('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));
    
    var miInit = { method: 'PATCH',
                headers: misCabeceras,
                body: data,
                //mode: 'cors',
                //cache: 'default' 
    };
    console.log(miInit,url);
      fetch(url,miInit)
        .then(response =>  {
          if (response.ok){
            return response.json() 
          }else{
            throw response.json()
          }
        }) 
        .then(result => {
          console.log("Exito",result);
          this.props.cerrar();
        }).catch((e) =>  {
          e.then(item=>{
            this.setState({errors:item["errors"]})
            console.error("Error",item)}
          )
        }) 
  }

  render() {
    return (
      <Grid container alignItems="center" style={{ justifyContent: 'center'}}>
        <Grid item sm={12} md={12} lg={9} align="center" mx="auto">
          <form id="form" onSubmit={this.props.edit?this.__handleSubmitEdit:this.__handleSubmitCreate}>
            <input hidden name="id" id="id" value={this.state.id} readOnly />
            <Card style={{ backgroundColor: '#EDEDED'}}>
              <CardHeader  title={this.props.edit?"Editar datos de empleado":"Agregar datos de empleado"} />
              <Divider variant="middle" />
              <CardContent>
                <Grid container spacing={2} justify="center">
                  <Grid item md={6} >
                    <TextField 
                      id="nombre"
                      value={this.state.nombre}
                      onChange={this.__handleChangeNombre}
                      type="text"
                      placeholder="Nombre"
                      label="Nombre del empleado"
                      variant="filled"
                      error={(typeof this.state.errors["nombre"] !== 'undefined')}
                      helperText={(typeof this.state.errors["nombre"]!=='undefined')?this.state.errors["nombre"]:""}
                      required
                      fullWidth
                    />
                  </Grid>
                  <Grid item md={6}>
                    <TextField
                      id="apellido"
                      placeholder="Apellido"
                      label="Apellido del empleado"
                      variant="filled"
                      type="text"
                      value={this.state.apellido}
                      onChange={this.__handleChangeApellido}
                      error={(typeof this.state.errors["apellido"] !== 'undefined')}
                      helperText={(typeof this.state.errors["apellido"]!=='undefined')?this.state.errors["apellido"]:""}
                      required
                      fullWidth
                    />
                  </Grid>
                </Grid>
                <Grid container spacing={2} justify="center">
                  <Grid item md={6}>
                    <TextField
                      id="email"
                      placeholder="Email"
                      label="Email del empleado"
                      variant="filled"
                      value={this.state.email}
                      type="email"
                      error={(typeof this.state.errors["email"] !== 'undefined')}
                      helperText={(typeof this.state.errors["email"]!=='undefined')?this.state.errors["email"]:""}
                      onChange={this.__handleChangeEmail}
                      required
                      fullWidth
                    />
                  </Grid>
                  <Grid item md={6}>
                    <TextField
                      id="telefono"
                      placeholder="Telefono"
                      label="Telefono del empleado"
                      variant="filled"
                      value={this.state.telefono}
                      type="tel"
                      onChange={this.__handleChangeTelefono}
                      inputProps={{
                        minLength:10,
                        maxLength:10,
                      }}
                      error={(typeof this.state.errors["telefono"] !== 'undefined')}
                      helperText={(typeof this.state.errors["telefono"]!=='undefined')?this.state.errors["telefono"]:""}
                      required
                      fullWidth
                    />
                  </Grid>
                </Grid>
              </CardContent>
              <Divider variant="middle"/>
              <CardActions >
                  <Grid container  justify="center" spacing={2}>
                    <Grid item>
                      <Button onClick={this.props.cerrar} variant="contained" color="secondary">CANCELAR</Button>
                    </Grid>
                    <Grid item>
                      <Button type="submit" variant="contained" color="primary" >{this.props.edit?"ACTUALIZAR":"ACEPTAR"}</Button>
                    </Grid>
                  </Grid>
              </CardActions>
            </Card>
          </form>
        </Grid>
      </Grid>
    );
  }
}

Form.propTypes = {
    url: PropTypes.string.isRequired,
};
  
export default Form;