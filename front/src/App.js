import React from "react";
import './App.css';
import { createMuiTheme } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';

import Grid from "@material-ui/core/Grid";
import Table from "./components/Table";

const theme = createMuiTheme({
  palette: {
    primary: {
      light: '#357a38',
      main: '#4caf50',
      dark: '#6fbf73',
      contrastText: '#fff',
    },
    secondary: {
      light: '#ab003c',
      main: '#f50057',
      dark: '#f73378',
      contrastText: '#000',
    },
  },
});
 function App() {    
    return (
      <ThemeProvider theme={theme}>
        <Grid container mx="auto">
          <Grid container item justify={"center"}>
            <h1>Emkode</h1>
          </Grid>
          <Grid container justify={"center"}>
            <Grid item md={10} sm={12} xs={12}>
              <Table url={"https://back-emkode.herokuapp.com/api/empleados"}/>
            </Grid>
          </Grid>
        </Grid>
      </ThemeProvider>
    )
  }
  
export default App;
