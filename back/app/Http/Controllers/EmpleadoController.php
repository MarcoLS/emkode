<?php

namespace App\Http\Controllers;

use App\Empleado;
use App\Http\Resources\EmpleadoCollection as EmpleadoResource;

use Illuminate\Http\Request;



class EmpleadoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filters=json_decode($request['filters'],true);
       
        return (new EmpleadoResource(Empleado::search($request['search'])->filter($filters)->paginate(isset($request["per_page"])?$request["per_page"]:10)));
        /* 

        //ddd($total,$total1,$total2,$total3,$total4);
            return (new EmpleadoResource(
                Empleado::search($request['search'],$campos)
                
                ->where("FACTURAS.ESTADOS_id","1")
                ->filter($filters,$campos)
                ->paginate((isset($request["per_page"])?$request["per_page"]:10))
            )); */
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request=$request->validate([
            'nombre'=> 'required|string|max:20',
            'apellido'=> 'required|string|max:20',
            'email'=> 'required|email|unique:empleados|max:30',
            'telefono'=> 'required|string|unique:empleados|max:10|min:10',
        ]);
        $empleado = new Empleado;
        $empleado->fill($request);

        return response()->json($empleado->save());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Empleado  $empleado
     * @return \Illuminate\Http\Response
     */
    public function show(Empleado $empleado)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Empleado  $empleado
     * @return \Illuminate\Http\Response
     */
    public function edit(Empleado $empleado)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Empleado  $empleado
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Empleado $empleado)
    {
        $request=$request->validate([
            'nombre'=> 'required|string|max:20',
            'apellido'=> 'required|string|max:20',
            'email'=> "required|email|unique:empleados,email,".$empleado->id."|max:30",
            'telefono'=> "required|string|max:10|min:10|unique:empleados,telefono,".$empleado->id,
        ]);

        return response()->json($empleado->update($request));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Empleado  $empleado
     * @return \Illuminate\Http\Response
     */
    public function destroy(Empleado $empleado)
    {
        return response()->json($empleado->delete());
    }
}
