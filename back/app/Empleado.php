<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empleado extends Model
{
    protected $fillable = ['nombre', 'apellido', 'email', 'telefono'];

    public function scopeFilter($query, $filter)
    {
        foreach ($filter as $item) {
            $query=$query->where($item["column"]["field"], 'like', "%".$item["value"]."%");
        }
        return $query;
    } 

    public function scopeSearch($query, $search)
    {   
        $fillable = ['nombre', 'apellido', 'email', 'telefono'];
        foreach ($fillable  as $item) {
            $query=$query->orWhere( $item , 'like', "%".$search."%");
        }
        return $query;
    } 
}
