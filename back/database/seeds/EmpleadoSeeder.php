<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class EmpleadoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('empleados')->insert([
            'nombre' => "Allan",
            'apellido' => "Poe",
            'email' => "allan_poe_09@email.com",
            'telefono' => "4435679083",
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('empleados')->insert([
            'nombre' => "Clive S.",
            'apellido' => "Lewis",
            'email' => "cs_lewis_98@email.com",
            'telefono' => "4437094524",
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('empleados')->insert([
            'nombre' => "JK",
            'apellido' => "Rowling",
            'email' => "jk_rowling_65@email.com",
            'telefono' => "4436122456",
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('empleados')->insert([
            'nombre' => "Cassandra",
            'apellido' => "Clare",
            'email' => "cassandra_clare_73@email.com",
            'telefono' => "4437972345",
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('empleados')->insert([
            'nombre' => "Stephen",
            'apellido' => "King",
            'email' => "stephen_king_47@email.com",
            'telefono' => "4438996523",
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
